<?php

/*
 * AmsWadController is a Controller file that receives the request dispatches to respective methods 
 * to handle the request. 
 * 
 * ‘view’ key is used to identify the URL request.
 */
date_default_timezone_set('Europe/Helsinki');
//mb_internal_encoding("UTF-8"); 
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
//This is a server using Windows
    $delim = ";";
    $slash = "\\";
} else {
//This is a server not using Windows!
    $delim = ":";
    $slash = "/";
}

define('APP_HOME', dirname(dirname((__FILE__))));
define('SLASH', $slash);

//echo '\nAplication_home:'.APP_HOME.' \n';
ini_set("include_path",  ini_get("include_path") .$delim . '/home/iordanov/php');

ini_set('include_path',  ini_get('include_path') . 
        $delim . APP_HOME . '/lib' . $delim . APP_HOME . '/lib/common'.
        $delim . APP_HOME . '/lib/ams'.$delim . APP_HOME . '/lib/ams/wad'.
        $delim . APP_HOME . '/lib/log4php' . $delim . APP_HOME . '/lib/log4php/configurators');



$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;
setcookie('cookiename', 'ws.ams.wad', time() + 60 * 60 * 24 * 365, '/', $domain, false);

//display_errors = On
ini_set("display_errors", "1");


ob_start();

header('Cache-control: private');
header("Content-type: application/json; charset=utf-8");
header('Access-Control-Allow-Origin: *');

$mn = "AmsWadController.php";

//require_once("AmsConnectionWad.php");
require_once("AmsLoggerWad.php");
//require_once("Functions.php");
require_once("AmsWadRestHandler.class.php");
AmsLoggerWad::logBegin($mn);
$view = "";
$id = null;
$iso2=null;
$region=null;
$subregion=null;
if (isset($_REQUEST["view"])) $view = $_REQUEST["view"];
if (isset($_REQUEST["id"])) $id = $_REQUEST["id"];
if (isset($_REQUEST["iso2"])) $iso2 = $_REQUEST["iso2"];
if (isset($_REQUEST["region"])) $region = $_REQUEST["region"];
if (isset($_REQUEST["subregion"])) $subregion = $_REQUEST["subregion"];

AmsLoggerWad::log($mn, "view=" . $view. ", id=" . $id);

/*
  controls the RESTful services URL mapping
 */
switch ($view) {

    case "region":
        // to handle REST Url /pcpd/
        $restHendler = new AmsWadRestHandler();
        if(isset($id)){
            $restHendler->RegionByName($id);
        }
        else
            $restHendler->Region();
        break;
    case "country":
        // to handle REST Url /mobile/show/<id>/
        $restHendler = new AmsWadRestHandler();
        if(isset($id)){
            $restHendler->County($id);
        }
        else if(isset($iso2)){
            $restHendler->CountyByIso2($iso2);
        }
        else if(isset($region)){
            $restHendler->CountyByRegion($region);
        }
        else if(isset($subregion)){
            $restHendler->CountyBySubRegion($subregion);
        }
        else{
        $restHendler->CountyAll();
        }
        
        break;

    case "" :
        AmsLoggerWad::log($mn, "No heandler for view: " . $view);
        break;
}

AmsLoggerWad::logEnd($mn);


