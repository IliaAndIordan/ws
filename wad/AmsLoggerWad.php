<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $amsLoggerWad;

require_once("LoggerBase.php");
Logger::configure(dirname(__FILE__).'/appender_pdo.properties');
/**
 * Description of Log
 *
 * @author izior
 */
class AmsLoggerWad extends LoggerBase {
    
    // <editor-fold defaultstate="collapsed" desc="__construct">
    public function __construct() {
        parent::__construct();
        $this->MN = "AmsLoggerWad: ";
        try {
            
            
            $this->logger = Logger::getRootLogger();
           
            $this->logger->debug("AmsLoggerWad init");
           
        } catch (Exception $ex) {
            echo "AmsLoggerWad Error: " . $ex."<br/>";
        }
        //logEndST($MN, $ST);
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function loggerWad(){
         global $amsLoggerWad;
        if(!isset($amsLoggerWad))
        {
            $amsLoggerWad = new AmsLoggerWad();
        }
        
        return $amsLoggerWad;
    }
    
    public static function logBegin($mn)
    {
        AmsLoggerWad::loggerWad()->begin($mn);
    }
    
     public static function logEnd($mn)
    {
        AmsLoggerWad::loggerWad()->end($mn);
    }
    
     public static function log($mn, $msg)
    {
        AmsLoggerWad::loggerWad()->debug($mn,$msg);
    }
    
    public static function logError($mn, $ex)
    {
        AmsLoggerWad::loggerWad()->error($mn);
    }
    // </editor-fold>
}
