<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: mlws
 * Module 				 	: package_name
 * Responsible for module 	: IordIord
 *
 * Filename               	: Connect.php
 *
 * Database System        	: ORCL, MySQL
 * Created from				: IordIord
 * Date Creation				: 14.09.2009
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 * 	 
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: Connect.php,v $
 * <br>---
 * <br>--- 
 *
 * ******************************** HEAD_END ************************************
 */


//require_once("Functions.php");
require_once("Response.class.php");
//require_once("LoggerBase.php");



class ConnectionBase {

    public $connection = null;

    // <editor-fold defaultstate="collapsed" desc="Construct">
    
    public function __construct($db_host, $db_user, $db_pass, $db_name, $db_port) {
        $MN = "common:ConnectionBase::__construct()";
        //$ST = logBegin($MN);
        
        $this->connection = new mysqli($db_host, $db_user, $db_pass, $db_name, $db_port);
        
        //echo 'include_path... '.ini_get('include_path').' <br/>';    
        $this->connection->query("SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ");
        //$this->connection->query("SET lower_case_table_names 1");
        //$this->connection->set_charset("usf8"); mysql:host=localhost;port=3307;dbname=testdb
        try {

            $this->connection->query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");

            
            // Will not affect $mysqli->real_escape_string();
            $this->connection->query("SET CHARACTER SET utf8");

            // But, this will affect $mysqli->real_escape_string();
            $this->connection->set_charset('utf8');

            $charset = $this->connection->character_set_name();
            //logDebug($MN, "db charset=".$charset);
            if (mysqli_connect_errno()) {
                $contentPage = "pp_maintenance";
                logDebug("$MN", "Database connect Error : "
                        . mysqli_connect_error($this->connection));

                header('Location: /dberror.html');
                //die();
                //header("Location: ".$url);
                ob_flush();
            }
        } catch (Exception $ex) {
            echo $ex;
            $contentPage = "pp_maintenance";
        }
        
        //logEndST($MN, $ST);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Json Query">
    
    public function dbExecuteSQLJson($strSQL, $logModel) {

        $MN = "ConnectionBase->dbExecuteSQLJson()";
        $logModel->logBegin($MN);

        $retArray = array();
        $idx = 0;
        
        $logModel->logDebug($MN, "SQL=" . $strSQL);

        try {
            $result = $this->connection->query($strSQL);
            //$logModel->logDebug($MN, "result =" . var_dump($result));
            if ($result) {
                while ($row = $result->fetch_assoc()) {
                    $retArray[$idx] = $row;
                    $idx++;
                    //$logModel->logDebug($MN, "row[" . $idx . "] =" . var_dump($row));
                }
                
                $result->close();
            }
        } catch (Exception $ex) {
            $logModel->logError($MN, $ex);
        }
        $logModel->logEnd($MN);
        return $retArray;
    }
    
    public function SelectJson($query, $bind_params_r, $logModel) {
        $MN = "common:Connect.SelectJson()";
        $logModel->logBegin($MN);


        $statement = $this->runPreparedQuery($query, $bind_params_r, $logModel);
        $statement->execute();
        $statement->store_result();
        $result_r = array();
        $result_r = $this->fetch($statement, $logModel);
//        $result_r = array();
//        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
//            $result_r[] = $row;
//        }
//        while($row = $result -> fetch_assoc()) {
//            $result_r[]= $row;    
//        }
        $i = 0;
//        while($assoc_array = $this->fetchAssocStatement($statement))
//        {
//            // logDebug($MN, "i=".i);
//            //logDebug($MN, "assoc_array=".prArr($assoc_array));
//            $result_r[$i] = $assoc_array;
//            //array_push($arr, $assoc_arra    y);
//            $i++;
//        }
        $statement->close();
        //$logModel->logDebug($MN, "result_r=".prArr($result_r));
        $logModel->logEnd($MN);
        return $result_r;
    }

    public function fetch($result, $logModel) {
        $array = array();

        if ($result instanceof mysqli_stmt) {
            $result->store_result();

            $variables = array();
            $data = array();
            $meta = $result->result_metadata();

            while ($field = $meta->fetch_field())
                $variables[] = &$data[$field->name]; // pass by reference

            call_user_func_array(array($result, 'bind_result'), $variables);

            $i = 0;
            while ($result->fetch()) {
                $array[$i] = array();
                foreach ($data as $k => $v)
                    $array[$i][$k] = $v;
                $i++;

                // don't know why, but when I tried $array[] = $data, I got the same one result in all rows
            }
        } elseif ($result instanceof mysqli_result) {
            while ($row = $result->fetch_assoc())
                $array[] = $row;
        }

        return $array;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Standart Query">
    
    public function dbExecuteSQL($strSQL, $logModel) {

        $MN = "ConnectionBase->dbExecuteSQL()";
        $logModel->logBegin($MN);

        $retArray = array();
        $idx = 0;
        
        //$strSQL = strtolower($strSQL);
        $logModel->logDebug("$MN", "SQL=" . $strSQL);

        $result = $this->connection->query($strSQL);

        if ($result) {
            while ($row = $result->fetch_row()) {
                $retArray[$idx] = $row;
                $idx++;
                //$logModel->logDebug($MN, "row[" . $idx . "] =" . prArr($row));
            }
            $result->close();
        }

        $logModel->logEnd($MN);
        return $retArray;
    }
    
    // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="Prepared Query">
    
    public function preparedSelect($query, $bind_params_r, $logModel) {
        $MN = "common:Connect.preparedSelect()";
        //$logModel->logBegin($MN);
        
        $select = $this->runPreparedQuery($query, $bind_params_r, $logModel);
        $fields_r = $this->fetchFields($select);

        foreach ($fields_r as $field) {
            $bind_result_r[] = &${$field};
        }

        $this->bindResult($select, $bind_result_r);

        $result_r = array();
        $i = 0;
        while ($select->fetch()) {

            foreach ($fields_r as $field) {

                $result_r[$i][$field] = $$field;
                /*
                  if(mb_detect_encoding($$field) === "UTF-8") {
                  $result_r[$i][$field] = $$field;
                  }
                  else{
                  $result_r[$i][$field] =  utf8_encode( $$field);
                  } */
            }
            $i++;
        }
        $select->close();
        //logDebug($MN, "result_r=".prArr($result_r));
        //logEndST($MN, $ST);
        return $result_r;
    }
    
    public function runPreparedQuery($query, $bind_params_r, $logModel) {
        $MN = "ConnectionBase->runPreparedQuery()";
        //$logModel->logBegin($MN);

        //$logModel->logDebug($MN, "SQL=" . $query);
        //$logModel->logDebug($MN, "get_server_info=".$this->connection->get_server_info());
        if (!$stmt = $this->connection->prepare($query)) {
            echo $query;
            echo"<br/>";
            echo "Prepare failed: (" . $this->connection->errno . ") " . $this->connection->error;
            return 0;
        }
        //logDebug($MN, "stmt=".$stmt);

        $this->bindParameters($stmt, $bind_params_r, $logModel);

        //logDebug($MN, "after bindParameters");
        if ($stmt->execute()) {
            //$logModel->logEnd($MN);
            return $stmt;
        } else {
            $logModel->logDebug($MN, mysqli_error($this->connection));
            $logModel->logDebug("$MN", "Error in query: " . $query);
            $logModel->logDebug("$MN", "Error: " . mysqli_error($this->connection));
            echo "Error: " . mysqli_error($this->connection);
            $logModel->logEnd($MN);
            return 0;
        }
    }
    
    private function bindParameters(&$stmt, &$bind_params_r, $logModel) {
        $MN = "ConnectionBase->bindParameters()";
        //$logModel->logBegin($MN);
        $tmp = array();
        foreach ($bind_params_r as $key => $value) {
            $tmp[$key] = &$bind_params_r[$key];
            //logDebug($MN, "key=" . $key . " value=" . $value);
        }

        if (!call_user_func_array([$stmt, "bind_param"], $tmp)) {
            $msgStr = "Param Bind failed, [" . implode(",", $bind_params_r) . "]:" . $bind_params_r[0] . " (" . $stmt->errno . ") " . $stmt->error;
            //$logModel->logDebug($MN, $msgStr);
            echo $msgStr;
        }

        //$logModel->logEnd($MN);
    }
    
     private function bindResult(&$obj, &$bind_result_r) {
        
        call_user_func_array(array($obj, "bind_result"), $bind_result_r);
        
    }
    
    private function fetchFields($selectStmt) {
        $MN = "common:Connect.fetchFields()";
        //$ST = logBegin($MN);
        $metadata = $selectStmt->result_metadata();
        $fields_r = array();
        while ($field = $metadata->fetch_field()) {
            $fields_r[] = $field->name;
        }
        //logEndST($MN, $ST);
        return $fields_r;
    }
    
    // </editor-fold>
}







/**
 * ******************************************************************************
 *                        Iordan Z Iordanov 2018
 * ******************************************************************************
 * */
