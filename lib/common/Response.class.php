<?php

// <editor-fold defaultstate="collapsed" desc="Response Class">

/**
 * Description of Response class
 *
 * @author IordIord
 */
class Response {
    
    public function __construct() {
        $this->status = "success";
        $this->data = array();
        $this->message = "";
    }
    
    public function __construct1(Exception $ex) {
        
        $this->status = "error";
        $this->data = array();
        $this->data["error_obj"] = $ex;
        $this->message = "Exception: ".(isset($ex)? $ex->getMessage()."See data error_obj parameter for details.":"An unknown error occurred.");
    }
    
    public function __construct2($status_, $data, $msg) {
        
        $this->status = $status_;
        if(!isset($this->data))
            $this->data=array();
        if(!isset($data))
            $data=array();
        $this->data = $data;
        $this->message = $msg;
    }
    
     /**
     * ***************************************************************************
     * Methods Declarations
     * ***************************************************************************
     */
    public function addData($parameter_name, $valueObj) {
        
        if(!isset($this->data))
            $this->data = array();
          $this->data[$parameter_name] = $valueObj;
    }
    
     public function toJSON() {
         
         if(!isset($this->status))
            $this->status = "success";
        return json_encode($this);
    }
    /*     
     ***************************************************************************
     * Parameters Declarations
     ***************************************************************************
     */

    public $status;
    public $data;
    public $message;

}

// </editor-fold>

